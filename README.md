# To go:
- `docker-compose build` then `docker-compose up`

- For test uncomment in back/Dockerfile `CMD	["npm", "--prefix", "/src/", "test"]` and comment on `CMD  ["node", "src/server.js"]`

- Front start on port 3000
- Back start on port 5000