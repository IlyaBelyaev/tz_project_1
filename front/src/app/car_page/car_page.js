import React, {Component} from 'react';
import Header from '../components/header/header';
import {Link} from 'react-router-dom';
import Chart from '../components/chart/chart';

class Car extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bets: [],
            endTime: null,
            selled: false
        };

        this.ref_cost = React.createRef();
        this.ref_description = React.createRef();
        this.ref_name = React.createRef();
        this.ref_preloader = React.createRef();
        this.ref_image = React.createRef();
        this.ref_timer = React.createRef();
    }

    render() {
        return [
            <div key={'car_1'} className="off-canvas-menu">
                <Header/>
                <div className="section" style={{marginTop: '100px'}}>
                    <div className="container">
                        <div className="row title-row">
                            <div className="col-md-2">
                                <h4 className="shop">Лот</h4>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 col-sm-6">
                                <div id="carousel" className="ml-auto mr-auto">
                                    <div className="card page-carousel">
                                        <div id="carouselExampleIndicators" className="carousel slide"
                                             data-ride="carousel">
                                            <div className="carousel-inner" role="listbox">
                                                <div className="carousel-item active">
                                                    <img ref={this.ref_image} className="d-block img-fluid" src=""
                                                         alt="Awesome Item"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Chart bets={this.state.bets}/>
                            </div>
                            <div className="col-md-5 col-sm-6">
                                <h2 ref={this.ref_name}>Loading...</h2>
                                <h4 className="price"><strong ref={this.ref_cost}>Loading...</strong></h4>
                                <h4 className="timer"><strong ref={this.ref_timer}>
                                </strong></h4>
                                <hr/>
                                <p ref={this.ref_description}>Loading...</p>
                                <div className="table-responsive">
                                    <table className="table">
                                        <thead>
                                        <tr>
                                            <th className="text-center">#</th>
                                            <th className="text-center">Имя</th>
                                            <th className="text-center">Ставка</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.bets.map((e, i) => {
                                                return (
                                                    <tr key={i}>
                                                        <td className="text-center">{i + 1}</td>
                                                        <td className="text-center">{e.user_name}</td>
                                                        <td className="text-center">{e.bet}    &#36;</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>


                                <hr/>
                                <div className="row">
                                    <div className="col-md-7 offset-md-5 col-sm-8">
                                        <button onClick={this.buy.bind(this)} className="btn btn-success btn-block btn-round">
                                            Купить
                                        </button>
                                    </div>
                                    <div style={{marginTop: '15px'}} className="col-md-7 offset-md-5 col-sm-8">
                                        <Link to="/" className="btn btn-info btn-block btn-round">Назад</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>,
            <div key={'car_2'} ref={this.ref_preloader} style={{
                position: 'fixed',
                left: 0,
                top: 0,
                zIndex: 999,
                width: '100%',
                height: '100%',
                overflow: 'visible',
                background: "#333 url('http://files.mimoymima.com/images/loading.gif') no-repeat center center"
            }}>
                <div></div>
            </div>
        ];
    }

    componentWillMount() {
        this.interval = setInterval(this.getDescriptionAndTimer.bind(this), 1000);
    }

    componentWillUnmount () {
        this.interval && clearInterval(this.interval);
        this.interval = false;
    }

    buy() {
        const that = this;
        // Беру id из URL параметра
        const id_car = window.location.search.split('id=')[1];
        const body = 'id_car=' + encodeURIComponent(id_car);

        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        fetch('http://localhost:5000/buy_car', {
            method: 'POST',
            credentials: 'include',
            headers: headers,
            body: body
        }).then(res => {
            if (res.ok){
                that.setState({selled: false});
            }
        });
    }

    getDescriptionAndTimer() {
        if (!this.state.selled) {
            const id = (new URL(document.location)).searchParams.get('id');
            const that = this;

            const xhr = new XMLHttpRequest();

            xhr.open('GET', `http://localhost:5000/get_lot_by_id?id=${id}`);
            xhr.onload = function () {
                try {
                    const response = JSON.parse(this.response);

                    // DESCRIPTION
                    that.ref_name.current.innerHTML = `Название: ${response.car_name}`;
                    that.ref_cost.current.innerHTML = `Цена: ${response.cost} &#36;`;
                    that.ref_description.current.innerHTML = `Описание: ${response.description}`;
                    that.ref_image.current.src = response.image;

                    // TABLE BETS
                    that.setState({bets: response.bets});

                    //TIMER
                    that.setState({endTime: parseInt(response.endTime)});
                    that.timer();

                    // Убрать pre-loader
                    that.ref_preloader.current.style.display = 'none';
                } catch (err) {
                    console.log(err);
                    //window.location.replace("/");
                }

            };

            xhr.send()
        }
    }

    timer() {
        if (Date.now() < this.state.endTime) {
            this.ref_timer.current.innerHTML = `Осталось времени: ${Math.round(-(Date.now() - this.state.endTime) / 1000)}`;
            this.setState({selled: false})
        } else {
            this.ref_timer.current.innerHTML = 'Лот продан';
            this.setState({selled: true})
        }
    }
}

export default Car;