import React, {Component} from 'react';
import '../../home/style.css';
import { Link } from 'react-router-dom';

export default class Auth extends Component {
    constructor(props) {
        super(props);

        this.state = {
            balance: null
        }
    }

    render() {
        return (
            <ul className="nav navbar-nav flex-row justify-content-between ml-auto">
                <li className="dropdown order-1">
                    <p style={{cursor: 'default'}} className="btn btn-neutral btn-round">Ваш баланс: {this.state.balance} 	&#36;</p>
                </li>
                <li className="dropdown order-2">
                    <Link to='/personal_area' className="btn btn-info btn-round">Личный кабинет</Link>
                </li>
                <li className="dropdown order-2">
                    <Link onClick={this.exit.bind(this)} to="/" className="btn  login_btn">Выход</Link>
                </li>
            </ul>
        )
    }

    componentDidMount(){
        // Update Balance
        this.interval = setInterval(function () {
            try {
                const xhr = new XMLHttpRequest();

                const that = this;
                xhr.open('GET', `http://localhost:5000/get_user_balance?id=${JSON.parse(window.localStorage.getItem('tokenDecode'))._id}`);

                xhr.onload = function () {
                    if (this.status === 200) {
                        that.setState({balance: this.response});
                    }
                };
                xhr.send();
            } catch (err) {}
        }.bind(this), 1000)
    }

    componentWillUnmount () {
        this.interval && clearInterval(this.interval);
        this.interval = false;
    }

    exit(event){
        window.localStorage.clear('token');
        window.localStorage.clear('tokenDecode');
        document.cookie = 'token=';
        this.props.handler(event);
    }
}