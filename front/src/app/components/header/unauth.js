import React, {Component} from 'react';
import jwt from 'jsonwebtoken';
import '../../home/style.css';

export default class Unauth extends Component {
    constructor(props) {
        super(props);
        this.sign_up_ref = React.createRef();
        this.sign_in_ref = React.createRef();

        this.state = {
            auth: null
        }
    }

    render() {
        return (
            <ul className="nav navbar-nav flex-row justify-content-between ml-auto">
                <li className="dropdown order-1">
                    <button type="button" id="dropdownMenu1" data-toggle="dropdown"
                            className="btn btn-outline-secondary dropdown-toggle login_btn">Вход</button>
                    <ul className="dropdown-menu dropdown-menu-right mt-2">
                        <li className="px-3 py-2">
                            <form className="form">
                                <div className="form-group">
                                    <input id="emailInput" placeholder="Почта"
                                           className="form-control form-control-sm sign_in_email" type="text"
                                           required=""/>
                                </div>
                                <div className="form-group">
                                    <input id="passwordInput" placeholder="Пароль"
                                           className="form-control form-control-sm sign_in_pwd" type="password"
                                           required=""/>
                                </div>
                                <div className="form-group">
                                    <a style={{color: 'white'}}
                                       className="btn btn-primary btn-block"
                                       ref={this.sign_in_ref}
                                    onClick={this.props.handler}>Войти</a>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
                <li className="dropdown order-2">
                    <button type="button" id="dropdownMenu2" data-toggle="dropdown"
                            className="btn dropdown-toggle login_btn">Регистрация
                    </button>
                    <ul className="dropdown-menu dropdown-menu-right mt-2">
                        <li className="px-3 py-2">
                            <form className="form">
                                <div className="form-group">
                                    <input id="emailInput" placeholder="Почта"
                                           className="form-control form-control-sm sign_up_email"
                                           type="text" required=""/>
                                </div>
                                <div className="form-group">
                                    <input id="passwordInput" placeholder="Имя"
                                           className="form-control form-control-sm sign_up_name" type="text"
                                           required=""/>
                                </div>
                                <div className="form-group">
                                    <input id="passwordInput" placeholder="Пароль"
                                           className="form-control form-control-sm sign_up_pwd" type="password"
                                           required=""/>
                                </div>
                                <div className="form-group">
                                    <a style={{color: 'white'}} data-toggle="dropdown" className="btn btn-primary btn-block"
                                       ref={this.sign_up_ref}
                                    >Зарегистрироваться</a>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        )
    }

    componentDidMount() {
        this.sign_up_ref.current.addEventListener('click', function () {
            const email = document.querySelector('.sign_up_email').value;
            const name = document.querySelector('.sign_up_name').value;
            const pwd = document.querySelector('.sign_up_pwd').value;

            const body = 'email=' + encodeURIComponent(email) +
                '&name=' + encodeURIComponent(name) +
                '&password=' + encodeURIComponent(pwd);

            const xhr = new XMLHttpRequest();

            xhr.open('POST', 'http://localhost:5000/sign_up');
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

            xhr.onload = function () {
                console.log(this.response);
            };

            xhr.send(body);

        });

        this.sign_in_ref.current.addEventListener('click', function (event) {
            const email = document.querySelector('.sign_in_email').value;
            const pwd = document.querySelector('.sign_in_pwd').value;

            const that = this;

            const body = 'email=' + encodeURIComponent(email) +
                '&password=' + encodeURIComponent(pwd);

            const xhr = new XMLHttpRequest();

            // xhr.open('POST', 'http://localhost:5000/secret');
            xhr.open('POST', 'http://localhost:5000/sign_in');
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            // xhr.setRequestHeader('Authorization', 'bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWVjYWUyOTQ0ZTkwYzRlYjhlZmE4NjMiLCJuYW1lIjoiSWx5YSIsImVtYWlsIjoiMTIzQGdtYWlsLmNvbSIsInJlZnJlc2hUb2tlbiI6ImV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUowZVhCbElqb2ljbVZtY21WemFDSXNJbWxoZENJNk1UVXlOVFV4TXpVNU1pd2laWGh3SWpveE5UTXpNamc1TlRreWZRLm5vVVR1bkY0RFlvYWQ3R0duUjZlTmNEYW9qWFJuUURJTGVuZFRnTFcyTGciLCJpYXQiOjE1MjU1MTM1OTIsImV4cCI6MTUyNTUxNTM5Mn0.F-HHCgPX28EOH9h0gyjNbLC9Ac71lWam4yW3MI2-4uw');

            xhr.onload = function () {
                if (this.status === 200) {
                    const token = JSON.parse(this.response).token;
                    localStorage.setItem('token', token);
                    localStorage.setItem('tokenDecode', JSON.stringify(jwt.decode(token)));
                    document.cookie = `token=${token}`;
                    that.props.handler(event);
                }
            };
            xhr.send(body);
        }.bind(this));
    }
}