import React, { Component } from 'react';
import Unauth from './unauth';
import Auth from './auth';
import '../../home/style.css';
import { Link } from 'react-router-dom';

export default class Header extends Component{
    constructor(props) {
        super(props);

        this.handler = this.handler.bind(this);

        this.state = {
            auth: false
        }
    }
    handler(e) {
        e.preventDefault();
        this.state.auth ? this.setState({auth:false}) : this.setState({auth:true});
    }
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <div className="container">
                    <Link to="/" className="navbar-brand" >Аукцион</Link>
                    <button className="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        &#9776;
                    </button>
                    <div className="collapse navbar-collapse" id="exCollapsingNavbar">
                        {
                            (window.localStorage.getItem('token') === null ||
                                window.localStorage.getItem('tokenDecode') === undefined) ?
                                <Unauth handler = {this.handler} /> :
                                <Auth handler = {this.handler}/>
                        }
                    </div>
                </div>
            </nav>
        )
    }
}