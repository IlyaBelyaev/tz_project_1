import React, {Component} from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';

class Chart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bets: []
        };
    }

    render() {
        return (
            <LineChart width={600} height={300} data={this.state.bets}
                       margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip/>
                <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{r: 8}}/>
            </LineChart>
        )
    }

    componentWillReceiveProps(newProps) {
        const data = [];
        newProps.bets.map( e => {
            data.push({name: `${new Date(parseInt(e.time)).getHours()}:${new Date(parseInt(e.time)).getMinutes()}`, pv: e.bet})
        });
        this.setState({bets: data});
    }
}

export default Chart;