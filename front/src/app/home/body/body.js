import React, {Component} from 'react';
import Item from './item';

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: null
        };

        this.ref_block = React.createRef();
        this.ref_preloader = React.createRef();
    }

    render() {
        return [
            <div key={'body_1'}>
                {this.state.items === null ? <div>Loading</div> :
                    <div className="cd-section section-white" id="pricing">
                        <div className="pricing-1 section">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6 ml-auto mr-auto text-center">
                                        <h2 className="title">Активные лоты</h2>
                                    </div>
                                </div>
                                <div className="row" ref={this.ref_block}>
                                    {this.state.items.map((e, i) => {
                                        return <Item
                                            key={i}
                                            color="orange"
                                            cost={e.cost}
                                            description={e.description}
                                            name={e.name}
                                            image={e.image}
                                            id={e.car_id}/>
                                    })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>,
            <div key={'body_2'} ref={this.ref_preloader} style={{
                position: 'fixed',
                left: 0,
                top: 0,
                zIndex: 999,
                width: '100%',
                height: '100%',
                overflow: 'visible',
                background: "#333 url('http://files.mimoymima.com/images/loading.gif') no-repeat center center"
            }}><div></div></div>
        ]
    }

    componentDidMount() {
        const that = this;

        this.interval = setInterval(function () {
            const count = 4;

            const xhr = new XMLHttpRequest();

            xhr.open('GET', `http://localhost:5000/get_lots?count=${count}`);
            xhr.onload = function () {
                const response = JSON.parse(this.response);

                that.setState({items: response});

                // Убрать pre-loader
                try{
                    that.ref_preloader.current.style.display = 'none';
                } catch(err){}
            };

            xhr.send()
        }, 1000);
    }

    componentWillUnmount () {
        this.interval && clearInterval(this.interval);
        this.interval = false;
    }
}

export default Body;