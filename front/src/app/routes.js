import React, { Component } from 'react';
import Home from './home/index';
import Car_page from './car_page/car_page';
import Personal_area from './personal_area/personal_area';
import Sell_car from './sell_car/sell_car';
import Add_car from './add_car/add_car';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

class Main extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/car' component={Car_page}/>
                    <Route path='/personal_area' component={Personal_area}/>
                    <Route path='/sell_car' component={Sell_car}/>
                    <Route path='/add_car' component={Add_car}/>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default Main;