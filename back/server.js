const express = require('express');
const config = require('./libs/config');
const log = require('./libs/log')(module);
const path = require('path');
const fs = require('fs');

// DB connection
require('./db/connect');

// PASSPORT
require('./libs/passport').jwt();

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});

// MIDDLEWARE
require('./libs/middleware')(app);

// PASSPORT MIDDLEWARE
require('./libs/passport').middleware(app);

// Routes
require('./routes/index')(app);

// Timer of lots
setInterval(require('./libs/timer_lots'), 1000);

app.listen(config.PORT, () => {
    log.info(`server has been started on port ${config.PORT}`)
});

module.exports = app;