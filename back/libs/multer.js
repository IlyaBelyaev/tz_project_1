const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

const multer  = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (!fs.existsSync(path.join(__dirname, './public'))){
            fs.mkdirSync(path.join(__dirname, './public'));
            fs.mkdirSync(path.join(__dirname, './public/img'));

            cb(null, path.join(__dirname, '../public/img'))
        } else {
            cb(null, path.join(__dirname, '../public/img'))
        }
    },
    filename: function (req, file, cb) {
        const hash = crypto.createHmac('sha256', Math.random().toString())
            .update(file.originalname)
            .digest('hex');
        cb(null, hash)
    }
});
const upload = multer({ storage: storage });

module.exports = upload;