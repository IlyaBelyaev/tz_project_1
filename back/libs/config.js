const ExtractJwt = require("passport-jwt").ExtractJwt;

module.exports = {
    PORT: 5000,

    mongoose: {
        url: "mongodb://localhost:27017/auct",
        urlDocker: "mongodb://mongo:27017/auct",
        options: {
            user: 'admin',
            pass: 'XDkDQnRrSf2wbWc4fPR7',
            auth: {authdb:"admin"}
        }
    },

    jwt: {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('bearer'),

        // Секретный ключ
        secretOrKey: '7DDJSu8UAvwfzRwjNgB5K4q4',
        // secretOrKey: 'secret',

        // Время жизни токена
        expiresIn: 60 * 30,

        expiresInRefreshToken: 60*60*24*90,
    },

    // Время ставки на аукционе в мс
    TIMER_AUCT: 120000,
    // Для теста
    // TIMER_AUCT: 2000,
};