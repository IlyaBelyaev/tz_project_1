const User = require('../db/User');
const Lots = require('../db/Lots');
const Car = require('../db/Cars');
const log = require('./log')(module);

module.exports = function () {
    Lots.find({}, {}, function (err, result) {
        result.map((elem) => {
            // Если время лота вышло
            if (elem.endTime && elem.endTime < Date.now().toString()){
                const winner_id = elem.bets[elem.bets.length-1].user_id;
                const last_bet = elem.bets[elem.bets.length-1].bet;
                const id_car = elem.car_id;

                // Добавляем деньги продавцу
                User.update({_id: elem.owner_id}, {$inc: {cash: last_bet}}, function (err) {
                    if (err) log.error(err);
                });

                // Меняем владельца авто
                Car.update({_id: elem.car_id}, {selling: false, owner_id: winner_id}, function (err) {
                    if (err) log.error(err);
                });

                // Удаляем авто из лотов
                Lots.findOneAndRemove({car_id: id_car}, function (err) {
                    if (err) log.error(err);
                });
            }
        })
    });
};


