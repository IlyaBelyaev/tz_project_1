const mongoose = require('mongoose');
const crypto = require('crypto');
const isEmail = require('validator/lib/isEmail');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    hashPassword: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    cash: {
        type: Number,
        default: 100
    },
    refreshToken: String
},{
    collection: "Users",
    versionKey: false
});

userSchema.pre('save', function(next) {
    this._id = new mongoose.Types.ObjectId();

    next();
});

userSchema.virtual('password')
    .set(function (password) {
        this.salt = Math.random() + 'shjgaiusfghasufhjasd9fas9jf9wfoiasnfffff9awfhjasjfsjfu9ajwhfan';
        this.hashPassword = this.encryptPassword(password);
    });

userSchema.methods = {
    encryptPassword: function (password) {
        return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
    },
    checkPassword: function (password) {
        return this.encryptPassword(password) === this.hashPassword;
    }
};

userSchema.statics = {
    checkEmail: async function (email) {
        if (!isEmail(email)){
            throw Error('Некорректная почта!');
        }

        const _email = await this.findOne({email: email}).exec();
        if (_email === null){
            return true;
        } else {
            throw Error('Почта уже зарегистрирована!');
        }
    },
    checkPassword: async function (password) {
        if (password.length < 8){
            throw Error('Пароль должен быть больше 8 символов!');
        } else {
            return true;
        }
    },
    checkName: async function (name) {
        if (name.length < 3){
            throw Error('Длинна имени должна быть больше 2 символов!');
        } else {
            return true;
        }
    },
};

module.exports = mongoose.model('User', userSchema);