const mongoose = require('mongoose');

const carSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    owner_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    selling: {
        type: Boolean,
        default: false
    },
    car_name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    }
},{
    collection: "Cars",
    versionKey: false
});

carSchema.pre('save', function(next) {
    this._id = new mongoose.Types.ObjectId();

    next();
});


module.exports = mongoose.model('Cars', carSchema);