const mongoose = require('mongoose');
const config = require('../libs/config').mongoose;

mongoose.connect(config.url)

    .catch(err => {
        mongoose.connect(config.urlDocker);
    });

module.exports = mongoose.connection;