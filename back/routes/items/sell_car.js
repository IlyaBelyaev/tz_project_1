const Car = require('../../db/Cars');
const Lots = require('../../db/Lots');
const config = require('../../libs/config');
const jwt = require('jsonwebtoken');
const log = require('../../libs/log')(module);

// Передеаем:
// id машины (id_car), цену(cost), id юзера(берется из куков), описание(description)
module.exports = function (req, res) {
    const id_car = req.body.id_car;
    const cost = req.body.cost;
    const description = req.body.description;

    // ID юзера из токена
    const user_id = jwt.decode(req.cookies.token)._id;

    // Проверка на принадлежность машины юзеру
    Car.findOne({_id: id_car})
        .where('owner_id').equals(user_id)
        .exec(function (err, result) {
            if (err) log.error(err);

            const owner_id = result.owner_id;
            const car_name = result.car_name;
            const image = result.image;
            const isSellingNow = result.selling;

            // Проверка на повторную продажу
            if (!isSellingNow){
                const newLot = new Lots({
                    owner_id: owner_id,
                    cost: cost,
                    description: description,
                    car_id: id_car,
                    car_name: car_name,
                    image: image,
                });

                newLot.save()
                    .then(result => res.send(result))
                    .catch(err => log.error(err));

                // Указывем, что машину на продаже
                Car.update({_id: id_car}, {selling: true}, function (err) {
                    if (err) log.error(err);
                })
            } else {
                res.header('Content-Type', 'text/plain');
                res.status(400).send('Машина уже на продаже');
            }
        })
};

