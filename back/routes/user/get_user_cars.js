const Cars = require('../../db/Cars');

module.exports = function (req, res) {
    const user_id = req.query.id;
    Cars.find({owner_id: user_id})
        .where('owner_id').equals(user_id)
        .then(result => res.send(result))
        .catch(err => {
            res.header('Content-Type', 'text/plain');
            res.status(400).send(err.message)
        });
};