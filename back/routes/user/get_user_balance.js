const User = require('../../db/User');

module.exports = function (req, res) {
    User.findById(req.query.id)
        .then(result => {
            res.header('Content-Type', 'text/plain');
            res.send(result.cash.toString())
        })
        .catch(err => {
            res.header('Content-Type', 'text/plain');
            res.status(400).send(err.message)
        });
};